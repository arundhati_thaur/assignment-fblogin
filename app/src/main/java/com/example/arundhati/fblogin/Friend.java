package com.example.arundhati.fblogin;

import android.graphics.Bitmap;

import java.net.URL;

/**
 * Created by Arundhati on 10/20/2015.
 */
public class Friend {

    String name,last_name,gender,first_name;
    URL picture;

    public void setPicture(URL picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getGender() {
        return gender;
    }

    public URL getPicture() {
        return picture;
    }

    public void setGender(String gender) {
        this.gender = gender;

    }


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }


}
