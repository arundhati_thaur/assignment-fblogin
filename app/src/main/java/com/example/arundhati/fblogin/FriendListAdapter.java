package com.example.arundhati.fblogin;

import android.content.Context;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Arundhati on 10/20/2015.
 */
public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.Holder> {
        List<Friend> data;
        Context context;
        int rowLayout;
        OnItemClickListener onItemClickListener;

        public FriendListAdapter(List<Friend> data, Context context, int rowLayout) {
            this.data = data;
            this.context = context;
            this.rowLayout = rowLayout;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(rowLayout, null);
            Holder viewHolder = new Holder(view);
            return viewHolder;
        }


    @Override
        public void onBindViewHolder(Holder holder, final int position) {

            holder.friendName.setText(data.get(position).getName());
            Picasso.with(context).load(String.valueOf(data.get(position).getPicture())).into(holder.image);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(position);
            }
        });

    }


        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return data == null ? 0 : data.size();
        }


        public class Holder extends RecyclerView.ViewHolder {
            TextView friendName;
            LinearLayout container;
            ImageView image;

            public Holder(View view){
                super(view);
                friendName = (TextView)view.findViewById(R.id.friendName);
                image = (ImageView)view.findViewById(R.id.image);
                container = (LinearLayout)view.findViewById(R.id.container);
            }
        }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}

