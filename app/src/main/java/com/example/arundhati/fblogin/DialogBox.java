package com.example.arundhati.fblogin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Arundhati on 10/20/2015.
 */
public class DialogBox extends DialogFragment{
    LayoutInflater inflater;
    View view;
    Friend friend;
    TextView firstName,lastName,gender;
    ImageView profilePic;


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_box_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        firstName = (TextView)view.findViewById(R.id.firstName);
        lastName = (TextView)view.findViewById(R.id.lastName);
        gender = (TextView)view.findViewById(R.id.gender);
        profilePic = (ImageView)view.findViewById(R.id.image);
//        profilepPic = (TextView)view.findViewById(R.id.pictureURL);
        friend = MainActivity.friends.get(MainActivity.positionId);
        firstName.setText(friend.getFirst_name());
        lastName.setText(friend.getLast_name());
        gender.setText(friend.getGender());
        Picasso.with(getActivity()).load(String.valueOf(friend.getPicture())).into(profilePic);
//        profilepPic.setText(friend.getPicture());
        builder.setView(view);
        return builder.create();
    }


}
