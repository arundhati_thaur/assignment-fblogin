package com.example.arundhati.fblogin;

/**
 * Created by Arundhati on 10/20/2015.
 */
public interface OnItemClickListener {
    void onItemClick(int position);
}
