package com.example.arundhati.fblogin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements OnItemClickListener, AppConstants {

    public static int positionId;
    CallbackManager callbackManager;
    RecyclerView friendlist;
    FriendListAdapter friendListAdapter;
    public static ArrayList<Friend> friends;
    Friend friendObj;
    DialogBox dialog;
    GraphRequest graphRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        friendlist = (RecyclerView) findViewById(R.id.friendList);
        friendlist.setLayoutManager(new LinearLayoutManager(this));
        friends = new ArrayList<>();
        friendListAdapter = new FriendListAdapter(friends, MainActivity.this, R.layout.friend_list_layout);
        friendlist.setAdapter(friendListAdapter);
        callbackManager = CallbackManager.Factory.create();
        final LoginButton loginButton = (LoginButton) findViewById(R.id.loginButton);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_friends"));
        friendListAdapter.setOnItemClickListener(this);
        getAccess();
        logout();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        getAccess();
                        logout();
                    }


                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemClick(int position) {
        dialog = new DialogBox();
        positionId = position;
        dialog.show(getFragmentManager(), DETAILS);
    }

    public void getAccess() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(),
                    "/me/friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Log.e(LOG, response.toString());

                            JSONObject json = response.getJSONObject();
                            JSONArray jarray = null;
                            try {
                                jarray = json.getJSONArray("data");
                                for (int i = 0; i < jarray.length(); i++) {

                                    JSONObject oneAlbum = jarray.getJSONObject(i);
                                    friendObj = new Friend();
                                    JSONObject pic = oneAlbum.getJSONObject("picture");
                                    JSONObject data = pic.getJSONObject("data");
                                    URL image_value = new URL(data.getString("url"));
                                    friendObj.setPicture(image_value);
                                    friendObj.setName(oneAlbum.getString("name"));
                                    friendObj.setGender(oneAlbum.getString("gender"));
                                    friendObj.setFirst_name(oneAlbum.getString("first_name"));
                                    friendObj.setLast_name(oneAlbum.getString("last_name"));
                                    friends.add(friendObj);
                                    friendListAdapter.notifyDataSetChanged();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,first_name,gender," +
                    "last_name,email,cover,picture.type(large)");
            graphRequest.setParameters(parameters);
            graphRequest.executeAsync();
        }
    }

    public void logout() {
        new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                       AccessToken currentAccessToken) {
                if (currentAccessToken == null) {
                    friends.clear();
                }
            }
        };
    }

}

